require('dotenv').config();
const _ = require('lodash');
const express = require('express');
const keenio = require('express-keenio');
const orgLoader = require('./loader');

const port = process.env.PORT || 8080;
const slackValidationToken = process.env.SLACK_TOKEN;
const teamDataUrl =
  'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml';
const teamDataFile = 'team.yml';

const app = express();
const teamData = orgLoader();

if (process.env.KEENIO_PID && process.env.KEENIO_KEY) {
  keenio.configure({
    client: {
      projectId: process.env.KEENIO_PID,
      writeKey: process.env.KEENIO_KEY,
    },
  });
  app.use(keenio.handleAll());
}

app.use(express.urlencoded());

app.get('/', (req, res) => {
  let response = {
    status: 200,
    results: teamData,
  };

  res.send(response);
});

app.get('/projects', (req, res) => {
  let results = teamData.filter(member => member.hasOwnProperty('projects'));

  res.send({
    status: 200,
    results,
  });
});

app.get('/groups', (req, res) => {
  queryEngineeringGroups(teamData);

  res.send({
    status: 200,
    results: results,
  });
});

app.get(
  [
    '/engineers-by-group',
    '/engineers-by-group/:group',
    '/engineers-by-group/:group/:action',
  ],
  (req, res) => {
    let results = queryEngineeringGroups(teamData);

    if (!!req.params.group) {
      results = results[req.params.group];
    }

    if (req.params.action === 'random') {
      results = results[_.random(results.length - 1)];
    }

    res.send({
      status: 200,
      results,
    });
  },
);

app.get('/departments', (req, res) => {
  const engOrg = buildHierarchy(teamData);

  res.send({
    status: 200,
    engOrg,
  });
});

app.post('/slack', (req, res) => {
  if (req.body.token !== slackValidationToken) {
    handleInvalidSlackToken(req, res);

    return false;
  }
  const command = req.body.text;
  let result;

  switch (command) {
    case 'groups':
      const engOrg = buildHierarchy(teamData);

      result = formatEngOrg(engOrg);
      res.send({
        text: `\`\`\`${result}\`\`\``,
        mrkdwn: true,
      });
      break;
    default:
      result = queryEngineeringGroup(req, res, true);
      res.send({
        attachments: [
          {
            fallback: `Your MR reviewer for the ${
              result.cleanedReportsToSlug
            } team is ${result.name}! https://gitlab.com/${result.gitlab}`,
            color: '#e65328',
            pretext: `Your MR reviewer for the ${
              result.cleanedReportsToSlug
            } team is:`,
            thumb_url: result.picture,
            title: `${result.name} (@${result.gitlab})`,
            title_link: `https://gitlab.com/${result.gitlab}`,
            text: result.role,
            fields: [
              {
                title: 'Location',
                value: `${result.locality}\n${result.country}`,
                short: true,
              },
              {
                title: 'Team',
                value: `${_.upperFirst(result.cleanedReportsToSlug)}`,
                short: true,
              },
            ],
            footer: 'GitLab Reviewer Roulette',
            footer_icon:
              'https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo-square.png',
          },
        ],
      });
      break;
  }
});

app.listen(port, () =>
  console.log(`Reviewer Roulette is running on port ${port}!`),
);

function handleInvalidSlackToken(req, res) {
  const errorMessage = `Invalid Slack token provided: ${req.body.token}`;
  console.error(errorMessage);
  res.status(500).send({
    status: 500,
    message: errorMessage,
  });
}

function queryEngineeringGroups(team) {
  let engineers = team.filter(
    member => member.departments.indexOf('Engineering') > -1,
  );
  let results = _.groupBy(engineers, 'cleanedReportsToSlug');

  return results;
}

function queryEngineeringGroup(req, res, random = false) {
  let results = queryEngineeringGroups(teamData);

  if (!!req.body.text) {
    results = results[req.body.text];
  }

  if (random) {
    results = results[_.random(results.length - 1)];
  }

  return results;
}

function buildHierarchy() {
  let results = {};
  let managers = teamData.filter(
    member =>
      member.departments.indexOf('Engineering') > -1 &&
      (member.role_slug.includes('director') ||
        member.role_slug.includes('manager') ||
        member.role_slug.includes('lead')),
  );

  // Create top hierarchy
  managers.forEach(manager => {
    if (manager.cleanedReportsToSlug === 'vp') {
      results[manager.cleanedRoleSlug] = [];
    }
  });

  // Create sub hierarchy
  managers.forEach(manager => {
    if (manager.cleanedReportsToSlug !== 'vp') {
      if (typeof results[manager.cleanedReportsToSlug] === 'undefined') {
        results[manager.cleanedReportsToSlug] = [];
      }
      results[manager.cleanedReportsToSlug].push(manager.cleanedRoleSlug);
    }
  });

  return results;
}

function formatEngOrg(org) {
  let response = [];

  for (const team in org) {
    _.isEmpty(org[team]) ? response.push(team) : response.push(org[team]);
  }

  return _.flatten(response).join('\n');
}

# gitlab-reviewer-roulette

## This project is abandoned and has been replaced with https://gitlab.com/gitlab-org/gitlab-ce/blob/master/danger/roulette/Dangerfile

A slack slash command to suggest GitLab reviewers for your Merge Requests!

## Usage
Use `/reviewerroulette groups` to see the available groups to pick a random reviewer for.

Use `/reviewerroulette <group>`, e.g., `/reviewerroulette frontend`

## Why?
With GitLab growing so quickly, it can be hard to keep track of who can review which parts of the code or UX.

While there are maintainers who have merge access, everyone is a reviewer, and this little script hopes to help with that!

## This code...it's terrible...
I know! It's great, right? Absolutely no planning went into this and I just immediately started coding. The first half of the code was built out to just see if I could figure out how to read the YAML and start serving the right bits of data out, depending on the request.

The second half is actually serving the useful data upon slack command requests.

The first half probably can be removed at this point :).

## It needs more "pop". When are you going to add more "pop"?
Check the [issues](https://gitlab.com/dennistang/reviewer-roulette/issues) page to see what's planned and if there's something you'd like it to do.